#!/usr/bin/env python

 
from datetime import datetime
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
     
TEMPLATE = open('datetimetemplate.html').read()
 
class SimpleServer(BaseHTTPRequestHandler):      
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
	current_time = datetime.now()
        self.wfile.write(TEMPLATE.format(date="Today's date is %s-%s-%s " % (current_time.day, current_time.month, current_time.year)))
 
 
server_address = ('', 80)
httpd = HTTPServer(server_address, SimpleServer)
 
print 'Starting httpd...'
httpd.serve_forever()

